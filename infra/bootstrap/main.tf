locals {
  region      = "europe-west4"
  environment = toset(["dev", "prod", "stage"])
}


resource "google_storage_bucket" "default" {
  for_each = local.environment

  project       = var.project_id
  name          = "country-population-bucket-tfstate-${each.value}"
  force_destroy = false
  location      = local.region
  storage_class = "STANDARD"
  versioning {
    enabled = true
  }

  encryption {
    default_kms_key_name = module.kms.keys["bucket-key"].id
  }
}

module "kms" {
  source     = "../../modules/kms"
  project_id = var.project_id
  keyring = {
    location = local.region
    name     = var.keyring_name
  }
  keyring_create = false
  keys = {
    bucket-key = {
      rotation_period = "604800s"
      purpose = "ENCRYPT_DECRYPT"
      version_template = {
        algorithm        = "GOOGLE_SYMMETRIC_ENCRYPTION"
        protection_level = "HSM"
      }
    }
  }
}