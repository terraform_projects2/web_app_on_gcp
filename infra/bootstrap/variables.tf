variable "project_id" {
  type = string
}

variable "sa_key" {
  type = string
}

variable "keyring_name" {
  type = string
}

variable "storage_bucket_service_agent" {
    type = string
}