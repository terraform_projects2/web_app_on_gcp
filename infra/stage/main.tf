locals {
  project_id = "country-population-statistics"
  region = "europe-west4"
  environment = "stage"
}

# module "vpc" {
#   source = "../../modules/vpc"

#   project_id = local.project_id
#   vpc_name   = "custom"
#   subnet = {
#     name          = "backend-subnet"
#     ip_cidr_range = "10.128.0.0/24"
#     region        = local.region
#   }

# }

# module "dev-firewall" {
#   source = "../../modules/factories/firewall-config"

#   project_id  = local.project_id
#   vpc_network = module.vpc.vpc_network_id
#   config_directories = [
#     "../firewall-rules/common",
#     "../firewall-rules/dev"
#   ]
# }

# module "compute_mig" {
#   source     = "../../modules/compute-mig"
#   project_id = local.project_id

#   instance_template = {
#     name                  = "webappserver-template"
#     description           = "This template is used to create MIGs."
#     tags                  = ["allow-health-check", "allow-http"]
#     machine_type          = "e2-standard-2"
#     source_image          = module.gce-container.source_image
#     disk_type             = "pd-balanced"
#     disk_size_gb          = 20
#     metadata              = { "gce-container-declaration" = module.gce-container.metadata_value }
#     labels                = { "container-vm" = module.gce-container.vm_container_label }
#     network               = module.vpc.vpc_network_id
#     subnetwork            = module.vpc.subnet_id
#     service_account_scope = ["cloud-platform"]
#   }

#   instance_group = {
#     name               = "webapp-mig"
#     zone               = local.zone
#     named_port_name    = "public-http"
#     named_port_port    = 8080
#     base_instance_name = "webapp-instance"
#     target_size        = 1
#   }

#   mig_autoscaler = {
#     name            = "webapp-autoscaler"
#     zone            = local.zone
#     max_replicas    = 5
#     min_replicas    = 1
#     cooldown_period = 60
#   }
# }

# # Handles the metadata that need to set up container
# module "gce-container" {
#   source  = "terraform-google-modules/container-vm/google"
#   version = "~> 2.0"

#   container = {
#     image          = "gcr.io/country-population-statistics/country-population-restapi@sha256:85e79dd9e1ea2a0ac2e6b054a86407e2bf53120e2c5eb26473a81987969a5fc0"
#     restart_policy = "Always"
#   }
# }

# module "networking" {
#   source           = "../../modules/networking"
#   project_id       = local.project_id
#   external_ip_name = "lb-static-ip"

#   forwarding_rule = {
#     name                  = "lb-forwarding-rule"
#     ip_protocol           = "TCP"
#     load_balancing_scheme = "EXTERNAL"
#     port_range            = "80"
#   }

#   backend_service = {
#     name                    = "backend-service"
#     protocol                = "HTTP"
#     port_name               = "public-http"
#     load_balancing_scheme   = "EXTERNAL"
#     timeout_sec             = 10
#     enable_cdn              = false
#     backend_group           = module.compute_mig.instance_group_manager_instance_group
#     backend_balancing_mode  = "UTILIZATION"
#     backend_capacity_scaler = 0.8
#   }
# }

# module "dns" {
#   source = "../../modules/dns"

#   project_id       = local.project_id
#   dns_managed_zone = "country-population-com"
#   dns_record_set = {
#     name    = "statistics"
#     type    = "A"
#     ttl     = 300
#     rrdatas = [module.networking.external_ip_address]
#   }
# }