locals {
  project_id  = "ninth-generator-408909"
  region      = "europe-west4"
  zone        = "europe-west4-a"
  environment = "dev"
}

module "vpc" {
  source = "../../modules/vpc"

  project_id = local.project_id
  vpc = {
    name = "custom"
  }
  subnet = {
    name          = "backend-subnet"
    ip_cidr_range = "10.128.0.0/24"
    region        = local.region
  }

}

module "dev-firewall" {
  source = "../../modules/factories/firewall-config"
  #source = "github.com/GoogleCloudPlatform/cloud-foundation-fabric/tree/master/blueprints/factories/net-vpc-firewall-yaml"

  project_id  = local.project_id
  vpc_network = module.vpc.vpc_network_id
  config_directories = [
    "../firewall-rules/common",
    "../firewall-rules/dev"
  ]
}

module "app-service-account" {
  source     = "../../modules/iam-service-account"
  project_id = local.project_id

  name         = "web-app-vm-sa"
  generate_key = true
  iam_project_roles = {
    "${local.project_id}" = [
      "roles/bigquery.jobUser",
      "roles/bigquery.dataViewer",
      "roles/storage.objectViewer"
    ]
  }
}

module "compute_mig" {
  source     = "../../modules/compute-mig"
  project_id = local.project_id

  instance_template = {
    name                  = "webappserver-template"
    description           = "This template is used to create MIGs."
    tags                  = ["allow-health-check", "allow-http"]
    machine_type          = "e2-standard-2"
    source_image          = module.gce-container.source_image
    disk_type             = "pd-balanced"
    disk_size_gb          = 20
    metadata              = { "gce-container-declaration" = module.gce-container.metadata_value }
    labels                = { "container-vm" = module.gce-container.vm_container_label }
    network               = module.vpc.vpc_network_id
    subnetwork            = module.vpc.subnet_id
    email                 = module.app-service-account.email
    service_account_scope = ["https://www.googleapis.com/auth/bigquery", "https://www.googleapis.com/auth/devstorage.read_only", ]
  }

  instance_group = {
    name               = "webapp-mig"
    zone               = local.zone
    named_port_name    = "public-http"
    named_port_port    = 8080
    base_instance_name = "webapp-instance"
    target_size        = 1
    version_name       = "primary"
    initial_delay_sec  = 300
  }

  mig_autoscaler = {
    name            = "webapp-autoscaler"
    zone            = local.zone
    max_replicas    = 5
    min_replicas    = 1
    cooldown_period = 60
  }
}

# Handles the metadata that need to set up container
module "gce-container" {
  source  = "terraform-google-modules/container-vm/google"
  version = "~> 2.0"

  container = {
    image          = "gcr.io/ninth-generator-408909/cp-images/country-population-restapi@sha256:57437186df8ec78a206fa02266771fd979e82cf7296b5d928ca37602c7be8e45"
    restart_policy = "Always"
  }
}

module "networking" {
  source           = "../../modules/networking"
  project_id       = local.project_id
  external_ip_name = "lb-static-ip"

  forwarding_rule = {
    name                  = "lb-forwarding-rule"
    ip_protocol           = "TCP"
    load_balancing_scheme = "EXTERNAL"
    port_range            = "80"
  }

  backend_service = {
    name                    = "backend-service"
    protocol                = "HTTP"
    port_name               = "public-http"
    load_balancing_scheme   = "EXTERNAL"
    timeout_sec             = 10
    enable_cdn              = false
    backend_group           = module.compute_mig.instance_group_manager_instance_group
    backend_balancing_mode  = "UTILIZATION"
    backend_capacity_scaler = 0.8
  }
}

module "dns" {
  source = "../../modules/dns"

  project_id       = local.project_id
  dns_managed_zone = "country-population-com"
  dns_record_set = {
    name    = "statistics"
    type    = "A"
    ttl     = 300
    rrdatas = [module.networking.external_ip_address]
  }
}

