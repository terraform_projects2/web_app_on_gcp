# create custom vpc network
resource "google_compute_network" "vpc_network" {
  project                 = var.project_id
  name                    = var.vpc.name
  auto_create_subnetworks = var.vpc.auto_create_subnetworks
}

# create subnet for backend
resource "google_compute_subnetwork" "custom_subnets" {
  project                  = var.project_id
  name                     = var.subnet.name
  provider                 = google-beta
  ip_cidr_range            = var.subnet.ip_cidr_range
  private_ip_google_access = var.subnet.private_ip_google_access
  region                   = var.subnet.region
  network                  = google_compute_network.vpc_network.id
}