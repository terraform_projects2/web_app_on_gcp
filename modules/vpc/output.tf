output "vpc_network_id" {
  value       = google_compute_network.vpc_network.id
  sensitive   = false
  description = "The ID of the created vpc network"
}

output "subnet_id" {
  value       = google_compute_subnetwork.custom_subnets.id
  sensitive   = false
  description = "The ID of the created vpc subnet"
}
