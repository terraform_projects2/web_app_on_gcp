variable "project_id" {
  type = string
}

variable "vpc" {
  type = object({
    name                    = string
    auto_create_subnetworks = optional(bool, false)
  })
}

variable "subnet" {
  type = object({
    name                     = string
    ip_cidr_range            = string
    region                   = string
    private_ip_google_access = optional(bool, true)
  })
}