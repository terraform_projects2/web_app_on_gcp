variable "project_id" {
  type = string
}

variable "dns_managed_zone" {
  type = string
}

variable "dns_record_set" {
  type = object({
    name    = string
    type    = string
    ttl     = number
    rrdatas = list(string)
  })
} 