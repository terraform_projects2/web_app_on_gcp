# Get the managed DNS zone
data "google_dns_managed_zone" "dns_zone" {
  project = var.project_id
  name    = var.dns_managed_zone
}

# Add Ip to the DNS 
resource "google_dns_record_set" "record_set" {
  project      = var.project_id
  name         = "${var.dns_record_set.name}.${data.google_dns_managed_zone.dns_zone.dns_name}"
  type         = var.dns_record_set.type
  ttl          = var.dns_record_set.ttl
  managed_zone = data.google_dns_managed_zone.dns_zone.name
  rrdatas      = var.dns_record_set.rrdatas
}