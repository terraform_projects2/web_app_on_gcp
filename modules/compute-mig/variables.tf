variable "project_id" {
  type = string
}

variable "instance_template" {
  type = object({
    name                  = string
    description           = string
    tags                  = list(string)
    machine_type          = string
    can_ip_forward        = optional(bool, false)
    source_image          = string
    disk_type             = string
    disk_size_gb          = number
    boot                  = optional(bool, true)
    metadata              = optional(map(string), {})
    labels                = optional(map(string), {})
    network               = string
    subnetwork            = string
    email                 = string
    service_account_scope = list(string)
  })
}

variable "instance_group" {
  type = object({
    name               = string
    zone               = string
    named_port_name    = string
    named_port_port    = number
    version_name       = string
    base_instance_name = string
    target_size        = number
    initial_delay_sec  = number
  })
}

variable "mig_autoscaler" {
  type = object({
    name            = string
    zone            = string
    max_replicas    = number
    min_replicas    = number
    cooldown_period = number
  })
}

variable "vm_health_check" {
  type = object({
    name                = string
    check_interval_sec  = number
    timeout_sec         = number
    healthy_threshold   = number
    unhealthy_threshold = number
    port                = string
  })
  default = {
    name                = "autohealing-health-check"
    check_interval_sec  = 5
    timeout_sec         = 5
    healthy_threshold   = 2
    unhealthy_threshold = 10
    port                = "8080"
  }
}