#create instance template
resource "google_compute_instance_template" "default" {
  project        = var.project_id
  name           = var.instance_template.name
  description    = var.instance_template.description
  tags           = var.instance_template.tags
  machine_type   = var.instance_template.machine_type
  can_ip_forward = var.instance_template.can_ip_forward

  disk {
    source_image = var.instance_template.source_image
    disk_type    = var.instance_template.disk_type
    disk_size_gb = var.instance_template.disk_size_gb
    boot         = var.instance_template.boot
  }

  metadata = var.instance_template.metadata

  labels = var.instance_template.labels

  network_interface {
    network    = var.instance_template.network
    subnetwork = var.instance_template.subnetwork
  }

  service_account {
    email  = var.instance_template.email
    scopes = var.instance_template.service_account_scope
  }
}

# MIG
resource "google_compute_instance_group_manager" "mig" {
  name     = var.instance_group.name
  project  = var.project_id
  provider = google-beta
  zone     = var.instance_group.zone

  named_port {
    name = var.instance_group.named_port_name
    port = var.instance_group.named_port_port
  }

  version {
    instance_template = google_compute_instance_template.default.id
    name              = var.instance_group.version_name
  }

  base_instance_name = var.instance_group.base_instance_name
  target_size        = var.instance_group.target_size

  auto_healing_policies {
    health_check      = google_compute_health_check.autohealing.id
    initial_delay_sec = var.instance_group.initial_delay_sec
  }
}

# set up autoscaling for MIG
resource "google_compute_autoscaler" "default" {
  provider = google-beta
  project  = var.project_id
  name     = var.mig_autoscaler.name
  zone     = var.mig_autoscaler.zone
  target   = google_compute_instance_group_manager.mig.id

  autoscaling_policy {
    max_replicas    = var.mig_autoscaler.max_replicas
    min_replicas    = var.mig_autoscaler.min_replicas
    cooldown_period = var.mig_autoscaler.cooldown_period
  }
}

resource "google_compute_health_check" "autohealing" {
  project             = var.project_id
  name                = var.vm_health_check.name
  check_interval_sec  = var.vm_health_check.check_interval_sec
  timeout_sec         = var.vm_health_check.timeout_sec
  healthy_threshold   = var.vm_health_check.healthy_threshold
  unhealthy_threshold = var.vm_health_check.unhealthy_threshold

  tcp_health_check {
    port = var.vm_health_check.port
  }
}