output "instance_group_manager_instance_group" {
  value     = google_compute_instance_group_manager.mig.instance_group
  sensitive = false
}
