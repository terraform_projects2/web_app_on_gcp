locals {
  firewall_rule_files = flatten(
    [
      for config_path in var.config_directories :
      concat(
        [
          for config_file in fileset(config_path, "**/*.yaml") :
          "${config_path}/${config_file}"
        ]
      )

    ]
  )

  firewall_rules = merge(
    [
      for config_file in local.firewall_rule_files :
      try(yamldecode(file(config_file)), {})
    ]...
  )
}

resource "google_compute_firewall" "custom" {
  for_each = local.firewall_rules

  project       = var.project_id
  name          = each.key
  provider      = google-beta
  direction     = each.value.direction
  network       = var.vpc_network
  source_ranges = try(each.value.source_ranges, each.value.direction == "INGRESS" ? [] : null)
  target_tags = try(each.value.target_tags, null)
  
  dynamic "allow" {
    for_each = { for block in try(each.value.allow, []) :
      "${block.protocol}-${join("-", block.ports)}" => {
        ports    = [for port in block.ports : tostring(port)]
        protocol = block.protocol
      }
    }
    content {
      protocol = allow.value.protocol
      ports    = allow.value.ports
    }
  }

  dynamic "deny" {
    for_each = { for block in try(each.value.deny, []) :
      "${block.protocol}-${join("-", block.ports)}" => {
        ports    = [for port in block.ports : tostring(port)]
        protocol = block.protocol
      }
    }
    content {
      protocol = deny.value.protocol
      ports    = deny.value.ports
    }
  }

}