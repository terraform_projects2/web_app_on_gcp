variable "config_directories" {
  description = "The source of the .yaml files which contain the firewall rules"
  type = list(string)
}

variable "vpc_network" {
  type = string
}

variable "project_id" {
  type = string
}