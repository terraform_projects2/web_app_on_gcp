variable "project_id" {
  type = string
}

variable "external_ip_name" {
  type = string
}

variable "forwarding_rule" {
  type = object({
    name                  = string
    ip_protocol           = string
    load_balancing_scheme = string
    port_range            = string
  })
}

variable "backend_service" {
  type = object({
    name                    = string
    protocol                = string
    port_name               = string
    load_balancing_scheme   = string
    timeout_sec             = number
    enable_cdn              = bool
    backend_group           = string
    backend_balancing_mode  = string
    backend_capacity_scaler = number
  })
}

variable "lb_health_check" {
  type = object({
    name                = string
    check_interval_sec  = number
    timeout_sec         = number
    healthy_threshold   = number
    unhealthy_threshold = number
    port                = string
  })
  default = {
    name                = "lb-hc"
    check_interval_sec  = 5
    timeout_sec         = 5
    healthy_threshold   = 2
    unhealthy_threshold = 2
    port                = "8080"
  }
}

