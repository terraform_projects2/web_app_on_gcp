# reserved IP address
resource "google_compute_global_address" "external_ip" {
  project  = var.project_id
  provider = google-beta
  name     = var.external_ip_name
}

# forwarding rule
resource "google_compute_global_forwarding_rule" "default" {
  project               = var.project_id
  name                  = var.forwarding_rule.name
  provider              = google-beta
  ip_protocol           = var.forwarding_rule.ip_protocol
  load_balancing_scheme = var.forwarding_rule.load_balancing_scheme
  port_range            = var.forwarding_rule.port_range
  target                = google_compute_target_http_proxy.target_proxy.id
  ip_address            = google_compute_global_address.external_ip.address
}

# http proxy
resource "google_compute_target_http_proxy" "target_proxy" {
  project  = var.project_id
  name     = "lb-target-http-proxy"
  provider = google-beta
  url_map  = google_compute_url_map.url_map.id
}

# url map
resource "google_compute_url_map" "url_map" {
  project         = var.project_id
  name            = "lb-url-map"
  provider        = google-beta
  default_service = google_compute_backend_service.backend.id
}

# backend service configuration
resource "google_compute_backend_service" "backend" {
  project               = var.project_id
  name                  = var.backend_service.name
  provider              = google-beta
  protocol              = var.backend_service.protocol
  port_name             = var.backend_service.port_name
  load_balancing_scheme = var.backend_service.load_balancing_scheme
  timeout_sec           = var.backend_service.timeout_sec
  enable_cdn            = var.backend_service.enable_cdn
  health_checks         = [google_compute_health_check.lb_hc.id]
  backend {
    group           = var.backend_service.backend_group
    balancing_mode  = var.backend_service.backend_balancing_mode
    capacity_scaler = var.backend_service.backend_capacity_scaler
  }
}

# health check for global load balancer
resource "google_compute_health_check" "lb_hc" {
  project             = var.project_id
  name                = var.lb_health_check.name
  provider            = google-beta
  check_interval_sec  = var.lb_health_check.check_interval_sec
  timeout_sec         = var.lb_health_check.timeout_sec
  healthy_threshold   = var.lb_health_check.healthy_threshold
  unhealthy_threshold = var.lb_health_check.unhealthy_threshold
  tcp_health_check {
    port = var.lb_health_check.port
  }
}