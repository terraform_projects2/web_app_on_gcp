output "external_ip_address" {
  value       = google_compute_global_address.external_ip.address
  sensitive   = false
  description = "External IP address of the HTTP lb"
}
